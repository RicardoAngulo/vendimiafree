<!DOCTYPE>
<%@ page language="java" contentType="text/html;" pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ include file="inicio.jsp" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.jqueryui.min.css">
        <link rel="stylesheet" type="text/css" href="css/button.css">
        <title>Vendimia</title>
    </head>
    <body>
        <button type="button" class="myButtonBlue" name="addarticulo" onclick="openPage('InsertarArticulos.jsp')" return true;">Nuevo Articulo</button>
        <br/>

        <%
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = null;
            /*parametros para la conexion*/
            String drivers = "com.mysql.jdbc.Driver";
            String urls = "jdbc:mysql://localhost:3306/ejercicios";
            String usuarios = "root";
            String claves = "";
                        String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://72.13.93.206:3307/ricardoangulo";
            String usuario = "ricardoangulo";
            String clave = "@Ngulo_codi";
        try{
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url,usuario,clave);
                conn.setAutoCommit(false);
                SQL = "SELECT id_articulo,descripcion,modelo,cantidad,precio,importe FROM articulos";
                ps = conn.prepareStatement(SQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = ps.executeQuery();
                conn.commit();
                out.println("<table id=\"example\" border=1 align=\"center\">");
                out.println("<tr>");
                out.println("<th>ID</th>");
                out.println("<th>Descripcion</th>");
                out.println("<th>Modelo</th>");
                out.println("<th>Cantidad</th>");
                out.println("<th>Precio</th>");
                out.println("<th>Importe</th>");
                out.println("<th>Opcion</th>");
                out.println("</tr>");
                while(rs.next()) {
                        out.println("<tr>");
                        out.println("<td align=\"center\">" + rs.getString("id_articulo") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("descripcion") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("modelo") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("cantidad") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("precio") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("importe") + "</td>");
                        out.println("<td align=\"center\"><a href=\"EditarArticulos.jsp?id="+rs.getString("id_articulo")+"\"><img src=\"img/editar.jpg\" style=\"width:30px; height:30px\"/></a></td>");
                        out.println("</tr>");
                } 
	
        } catch (SQLException exQL) {
                conn.rollback();
                out.println("Error SQL: " + exQL.getMessage());	
        } catch (Exception ex) {
                conn.rollback();
                out.println("Error: " + ex.getMessage());
        } finally {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (conn != null) conn.close();
        } // end try
        %>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.jqueryui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#example').DataTable();
            });

            function openPage(pageURL)
            {
                window.location.href = pageURL;
            }
        </script>
    </body>
</html>