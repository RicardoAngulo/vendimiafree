<%@ page import="java.io.*,java.util.*,java.net.*,java.sql.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ include file="inicio.jsp" %>
<head>
    <title>Vendimia</title>
    <link rel="stylesheet" type="text/css" href="css/DivValidation.css">
    <link rel="stylesheet" type="text/css" href="css/input.css">
</head>
<body>
    <form action="" method="post" name="formulario" class="formulario" method="post">
        <div class="diventas">Registro de Clientes</div>
        <div class="diventascuerpo">
            <div class="form-style-2">
                <label for="field1">
                    <span>Nombre <span class="required"></span></span>
                    <input type="text" class="input-field" name="nombre" id="nombre" placeholder="Ingresa Nombre" />
                </label>
                <label for="field2">
                    <span>Apellido Paterno <span class="required"></span></span>
                    <input type="text" class="input-field" name="apellido_paterno" id="apellido_paterno" placeholder="Ingresa Apellido Paterno" />
                </label>
                <label for="field3">
                    <span>Apellido Materno <span class="required"></span></span>
                    <input type="text" class="input-field" name="apellido_materno" id="apellido_materno" placeholder="Ingresa Apellido Materno" />
                </label>
                <label for="field4">
                    <span>RFC <span class="required"></span></span>
                    <input type="text" class="input-field" name="rfc" id="rfc" placeholder="Ingresa RFC" />
                </label>
                <div class="before"></div>
                <div class="ojo"></div>
                <div class="sucess"></div>
                <div class="error"></div>
                <button name="grabar" class="grabar">Guardar Cliente</button>
            </div>
        </div>
    </form>
    <script src='js/jquery.js'></script>
    <script src='js/jquery-1.4.2.min.js'></script>
    <script type="text/javascript" src="js/addcliente.js"></script>
    <script src="js/JqueryValidateForm.js"></script>
    <script src="js/JqueryValidate.js"></script>
</body>