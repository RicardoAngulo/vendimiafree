<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Prueba de JSP para DevTroce</title>
</head>
<body>
 
<%
Connection conn = null;
PreparedStatement ps = null;
ResultSet rs = null;
String SQL = null;
 
try{
	Class.forName("com.mysql.jdbc.Driver").newInstance();
	conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/ejercicios","root","");
	conn.setAutoCommit(false);
	SQL = "Select nombre, edad, statura from mitabla";
	ps = conn.prepareStatement(SQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	rs = ps.executeQuery();
	conn.commit();
	out.println("<h1 align=\"center\"> Lista de Usuarios </h1>");
	out.println("<table border=1 align=\"center\">");
	out.println("<tr>");
	out.println("<th>Nombres</th>");
	out.println("<th>Edades</th>");
	out.println("<th>Statura</th>");
	out.println("</tr>");
	while(rs.next()) {
		out.println("<tr>");
		out.println("<td>" + rs.getString("nombre") + "</td>");
		out.println("<td>" + rs.getString("edad") + "</td>");
		out.println("<td>" + rs.getString("statura") + "</td>");
		out.println("</tr>");
	} 
	out.println("</table>");
} catch (SQLException exQL) {
	conn.rollback();
	out.println("Error SQL: " + exQL.getMessage());	
} catch (Exception ex) {
	conn.rollback();
	out.println("Error: " + ex.getMessage());
} finally {
	if (rs != null) rs.close();
	if (ps != null) ps.close();
	if (conn != null) conn.close();
} // end try
%>
</body>
</html>