<%@ page language="java" contentType="text/html; charset=windows-1256" pageEncoding="windows-1256" %>
<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Login</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
        <link rel='stylesheet prefetch' href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900|RobotoDraft:400,100,300,500,700,900'>
        <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css'>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/DivValidation.css">
    </head>
    <body>

        <!-- Form Mixin-->
        <!-- Input Mixin-->
        <!-- Button Mixin-->
        <!-- Pen Title-->
        <div class="pen-title">
        </div>
        <!-- Form Module-->
        <div class="module form-module">
            <div class="toggle"><i class="fa fa-times fa-pencil"></i>
                <!--<div class="tooltip">Click</div>-->
            </div>
            <div class="form">
                <h2>Login to your account</h2>
                <form name="formulario" class="formulario" method="POST">
                    <label for="forUsername" id="forUsername">Username:</label>
                    <input type="text" placeholder="Username" name="Username" class="Username" id="Username" />
                    <label for="forPassword" id="forPassword">Password:</label>
                    <input type="password" placeholder="Password" name="Password" class="Password" id="Password" />
                    <div class="before"></div>
                    <div class="ojo"></div>
                    <div class="sucess"></div>
                    <div class="error"></div>
                    <button class="login">Login</button>
                </form>
            </div>
            <div class="form">
                <h2>Create Count</h2>
                <form>
                    <input type="text" placeholder="Usernames"/>
                    <input type="password" placeholder="Passwords"/>
                    <button>Register</button>
                </form>
            </div>
        </div>
        <script src='js/jquery.js'></script>
        <script src='js/jquery-1.4.2.min.js'></script>
        <script src="js/index.js"></script>
        <script src="js/JqueryValidateForm.js"></script>
        <script src="js/JqueryValidate.js"></script>
    </body>
</html>
