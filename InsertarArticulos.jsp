<%@ page import="java.io.*,java.util.*,java.net.*,java.sql.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ include file="inicio.jsp" %>
<head>
    <title>Vendimia</title>
    <link rel="stylesheet" type="text/css" href="css/DivValidation.css">
    <link rel="stylesheet" type="text/css" href="css/input.css">
</head>
<body>
    <form action="" method="post" name="formulario" class="formulario" method="post">
        <div class="diventas">Registro de Articulos</div>
        <div class="diventascuerpo">
            <div class="form-style-2">
                <label for="field1">
                    <span>Descripcion <span class="required"></span></span>
                    <input type="text" class="input-field" name="descripcion" id="descripcion" placeholder="Ingresa Descripcion" />
                </label>
                <label for="field2">
                    <span>Modelo <span class="required"></span></span>
                    <input type="text" class="input-field" name="modelo" id="modelo" placeholder="Ingresa Modelo" />
                </label>
                <label for="field3">
                    <span>Cantidad <span class="required"></span></span>
                    <input type="text" class="input-field" name="cantidad" id="cantidad" placeholder="Ingresa Cantidad" />
                </label>
                <label for="field4">
                    <span>Precio <span class="required"></span></span>
                    <input type="text" class="input-field" name="precio" id="precio" placeholder="Ingresa Precio" />
                </label>
                <label for="field5">
                    <span>Importe <span class="required"></span></span>
                    <input type="text" class="input-field" name="importe" id="importe" placeholder="Ingresa Importe" />
                </label>
                <div class="before"></div>
                <div class="ojo"></div>
                <div class="sucess"></div>
                <div class="error"></div>
                <button name="grabar" class="grabar">Guardar Articulo</button>
            </div>
        </div>
    </form>
    <script src='js/jquery.js'></script>
    <script src='js/jquery-1.4.2.min.js'></script>
    <script type="text/javascript" src="js/numeric.js"></script>
    <script type="text/javascript" src="js/addarticulos.js"></script>
    <script src="js/JqueryValidateForm.js"></script>
    <script src="js/JqueryValidate.js"></script>
</body>