<!DOCTYPE>
<%@ page language="java" contentType="text/html;" pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ include file="inicio.jsp" %>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.jqueryui.min.css">
        <link rel="stylesheet" type="text/css" href="css/button.css">
        <title>Vendimia</title>
    </head>
    <body>
        <button type="button" class="myButtonBlue" name="addcliente" onclick="openPage('AgregarVenta.jsp')" return true;">Nueva Venta</button>
        <br/>
        <%
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = null;
            /*parametros para la conexion*/
            String drivers = "com.mysql.jdbc.Driver";
            String urls = "jdbc:mysql://localhost:3306/ejercicios";
            String usuarios = "root";
            String claves = "";
                        String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://72.13.93.206:3307/ricardoangulo";
            String usuario = "ricardoangulo";
            String clave = "@Ngulo_codi";
        try{
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url,usuario,clave);
                conn.setAutoCommit(false);
                SQL = "SELECT ventas.id_venta,ventas.folio_venta,clientes.id_cliente,CONCAT(clientes.nombre,' ',clientes.apellido_paterno,' ',clientes.apellido_materno)AS nombre,ventas.total,ventas.fecha,ventas.status FROM ventas INNER JOIN clientes ON ventas.clave_cliente=clientes.id_cliente";
                ps = conn.prepareStatement(SQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = ps.executeQuery();
                conn.commit();
                out.println("<table id=\"example\" border=1 align=\"center\">");
                out.println("<tr>");
                out.println("<th>Folio Venta</th>");
                out.println("<th>Clave Cliente</th>");
                out.println("<th>Nombre</th>");
                out.println("<th>Total</th>");
                out.println("<th>Fecha</th>");
                out.println("<th>Status</th>");
                out.println("<th>Opcion</th>");
                out.println("</tr>");
                while(rs.next()) {
                        out.println("<tr>");
                        out.println("<td align=\"center\">" + rs.getString("folio_venta") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("id_cliente") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("nombre") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("total") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("fecha") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("status") + "</td>");
                        out.println("<td align=\"center\"><a href=\"EditarClientes.jsp?id="+rs.getString("id_venta")+"\"><img src=\"img/editar.jpg\" style=\"width:30px; height:30px\"/></a></td>");
                        out.println("</tr>");
                } 
                out.println("</table>");
        } catch (SQLException exQL) {
                conn.rollback();
                out.println("Error SQL: " + exQL.getMessage());	
        } catch (Exception ex) {
                conn.rollback();
                out.println("Error: " + ex.getMessage());
        } finally {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (conn != null) conn.close();
        } // end try
        %>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.jqueryui.min.js"></script>
        <script type="text/javascript">
             $(document).ready(function () {
                 $('#example').DataTable();
             });


             function openPage(pageURL)
             {
                 window.location.href = pageURL;
             }
        </script>
    </body>
</html>