<!DOCTYPE>
<%@ page language="java" contentType="text/html;" pageEncoding="ISO-8859-1"%>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.jqueryui.min.css">
        <link rel="stylesheet" type="text/css" href="css/button.css">
        <title>Vendimia</title>
    </head>
    <body>
        <br/>
        <%
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String SQL = null;
            /*parametros para la conexion*/
            String drivers = "com.mysql.jdbc.Driver";
            String urls = "jdbc:mysql://localhost:3306/ejercicios";
            String usuarios = "root";
            String claves = "";
                        String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://72.13.93.206:3307/ricardoangulo";
            String usuario = "ricardoangulo";
            String clave = "@Ngulo_codi";
        try{
                Class.forName(driver).newInstance();
                conn = DriverManager.getConnection(url,usuario,clave);
                conn.setAutoCommit(false);
                SQL = "SELECT venta_detalle.id_venta,articulos.id_articulo,articulos.descripcion,articulos.modelo,venta_detalle.cantidad,articulos.precio,(articulos.precio*venta_detalle.cantidad) AS importe FROM venta_detalle INNER JOIN articulos ON venta_detalle.id_producto=articulos.id_articulo WHERE venta_detalle.id_venta=1";
                ps = conn.prepareStatement(SQL, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                rs = ps.executeQuery();
                conn.commit();
                out.println("<form name=\"form\">");
                out.println("<table id=\"example\" border=1 align=\"center\" width=\"100%\">");
                out.println("<tr>");
                out.println("<th>Descripcion</th>");
                out.println("<th>Modelo</th>");
                out.println("<th>Cantidad</th>");
                out.println("<th>Precio</th>");
                out.println("<th>Importe</th>");
                out.println("<th>Opcion</th>");
                out.println("</tr>");
                int valor=0;
                while(rs.next()) {
                    valor++;
                        out.println("<tr>");
                        out.println("<td align=\"center\">" + rs.getString("descripcion") + "</td>");
                        out.println("<td align=\"center\">" + rs.getString("modelo") + "</td>");
                        out.println("<td align=\"center\"><input type=\"text\" class=\"cantidad\" id=\"cantidad\" onblur=\"if(this.value == ''){this.value='0'}\"  onKeyUp=\"suma("+valor+");\" value="+rs.getString("cantidad")+"></td>");
                        out.println("<td align=\"center\" class=\"precio\" id=\"precio\" onblur=\"if(this.value == ''){this.value='0'}\"  onKeyUp=\"suma("+valor+");\">" + rs.getString("precio") + "</td>");
                        out.println("<td align=\"center\" class=\"importe\" id=\"importe\" onblur=\"if(this.value == ''){this.value='0'}\"  onKeyUp=\"suma("+valor+");\">" + rs.getString("importe") + "</td>");
                        out.println("<td align=\"center\"><a href=\"VentaDetalleDelete.jsp?id_venta="+rs.getString("id_venta")+"&id_producto="+rs.getString("id_articulo")+"\"><img src=\"img/borrar.jpg\" style=\"width:30px; height:30px\"/></a></td>");
                        out.print("<button name=\"cancelar\" id=\"cancelar\" class=\"myButtonGreen\" style=\"position: absolute; top: 560px; left:1270px;\">Cancelar</button>");
        out.print("<button name=\"guardar\" id=\"guardar\" class=\"myButtonGreen\" style=\"position: absolute; top: 560px; left:1150px;\">Guardar</button>");
                        out.println("</tr>");
                } 
                out.println("</table>");
                out.println("</form>");
        } catch (SQLException exQL) {
                conn.rollback();
                out.println("Error SQL: " + exQL.getMessage());	
        } catch (Exception ex) {
                conn.rollback();
                out.println("Error: " + ex.getMessage());
        } finally {
                if (rs != null) rs.close();
                if (ps != null) ps.close();
                if (conn != null) conn.close();
        } // end try
        %>
        <script src='js/jquery.js'></script>
        <script src='js/jquery-1.4.2.min.js'></script>
        <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/dataTables.jqueryui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#example').DataTable();
            });


            function openPage(pageURL)
            {
                window.location.href = pageURL;
            }

            function suma(var su) {
                var sum1 = document.getElementById("cantidad1" + su);
                var sum2 = document.getElementById("precio1" + su);
                var div = document.getElementById("importe1" + su);
                resultado = parseInt(sum1.value) + parseInt(sum2.value);
                div.innerHTML = parseInt(resultado);
            }
        </script>
    </body>
</html>