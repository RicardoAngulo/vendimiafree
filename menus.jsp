<%@ page import="java.util.*" %>
<%@ page import="java.text.SimpleDateFormat"%>
  
<%
   Date dNow = new Date();
   SimpleDateFormat ft = 
   new SimpleDateFormat ("yyyy-MM-dd");
   String currentDate = ft.format(dNow);
%>
<html>
  <head>
    <title>Menu Desplegable</title>
    <style type="text/css">
      
      
    </style>
    <link rel="stylesheet" type="text/css" href="css/menus.css">
  </head>
  <body>
    <div id="header">
       <!-- Aqui estamos iniciando la nueva etiqueta nav -->
        <ul class="nav">
          <li><a href="">Inicio</a>
            <ul>
              <li><a href="">Submenu1</a></li>
              <li><a href="">Submenu2</a></li>
              <li><a href="">Submenu3</a></li>
              <li><a href="">Submenu4</a>
                <ul>
                  <li><a href="">Submenu1</a></li>
                  <li><a href="">Submenu2</a></li>
                  <li><a href="">Submenu3</a></li>
                  <li><a href="">Submenu4</a></li>
                </ul>
              </li>
            </ul>
          </li>
          <li><a href="#"><div id="fecha"><%=currentDate%></div></a></li>
        </ul>
      <!-- Aqui estamos cerrando la nueva etiqueta nav -->
    </div>
  </body>
</html> 