
$(document).ready(function () {
    $(".before").hide();
    $(".sucess").hide();
    $(".error").hide();
    $(".ojo").hide();
    $('.grabar').click(function () {
        if ($("#nombre_producto").val() == "") {
            alert("Ingrese Producto");
            $("#nombre_producto").focus();
            return false;
        } else {
             var dataString = 'nombre_producto=' + $('#nombre_producto').val() + '&nombre_cliente=' + $('#nombre_cliente').val();
            $.ajax({
                type: "POST",
                url: "addventadetalle.jsp",
                data: dataString,
                beforeSend: function () {
                    $(".before").show();
                    $(".before").html("<strong>Enviando datos</strong>");
                }, success: function (data) {
                    if (data == 1) {
                        $(".before").hide();
                        $(".sucess").show();
                        $(".sucess").html("<strong>Bien Hecho. El articulo ha sido registrado correctamente</strong>");
                        setTimeout(function () {
                            window.location = "AgregarVenta.jsp";
                        }, 3000);
                    } else {
                        $(".before").hide();
                        $(".ojo").show();
                        $(".ojo").html("<strong>Error en</strong> :" + data);
                    }
                }, error: function (data) {
                    $(".before").hide();
                    $(".sucess").hide();
                    $(".error").show();
                    $(".error").html("<strong>Ocurrio un error</strong> " + data);
                }
            });
            return false;
        }
    });
});
