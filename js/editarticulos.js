
$(document).ready(function () {
    $(".before").hide();
    $(".sucess").hide();
    $(".error").hide();
    $(".ojo").hide();
    $('#guardar').click(function () {
        if ($("#descripcion").val() == "") {
            alert("Ingrese Descripcion");
            $("#descripcion").focus();
            return false;
        } else if ($("#modelo").val() == "") {
            alert("Ingrese Modelo");
            $("#modelo").focus();
            return false;
        } else if ($("#cantidad").val() == "") {
            alert("Ingrese Cantidad");
            $("#cantidad").focus();
            return false;
        } else if ($("#precio").val() == "") {
            alert("Ingrese Precio");
            $("#precio").focus();
            return false;
        } else if ($("#importe").val() == "") {
            alert("Ingrese Importe");
            $("#importe").focus();
            return false;
        } else {

            var dataString = 'descripcion=' + $('#descripcion').val() + '&modelo=' + $('#modelo').val() + '&cantidad=' + $('#cantidad').val() + '&precio=' + $('#precio').val() + '&importe=' + $('#importe').val()+ '&id_articulo=' + $('#id_articulo').val();
            $.ajax({
                type: "POST",
                url: "editarticulos.jsp",
                data: dataString,
                beforeSend: function () {
                    $(".before").show();
                    $(".before").html("<strong>Enviando datos</strong>");
                }, success: function (data) {
                    if (data == 1) {
                        $(".before").hide();
                        $(".sucess").show();
                        $(".sucess").html("<strong>Bien Hecho. El producto ha sido actualizado correctamente</strong>");
                        setTimeout(function () {
                            window.location = "SeleccionarArticulos.jsp";
                        }, 3000);
                    } else {
                        $(".before").hide();
                        $(".ojo").show();
                        $(".ojo").html("<strong>Error en</strong> :" + data);
                    }
                }, error: function (data) {
                    $(".before").hide();
                    $(".sucess").hide();
                    $(".error").show();
                    $(".error").html("<strong>Ocurrio un error</strong>");
                }
            });
            return false;
        }
    });

    $('#cantidad').numeric(",");//entero
    $('#precio').numeric();//decimal
    $('#importe').numeric();//decimal
});
   