
$(document).ready(function () {
    $(".before").hide();
    $(".sucess").hide();
    $(".error").hide();
    $(".ojo").hide();
    $('#guardar').click(function () {
        if ($("#nombre").val() == "") {
            alert("Ingrese Nombre");
            $("#nombre").focus();
            return false;
        } else if ($("#apellido_paterno").val() == "") {
            alert("Ingrese Apellido Materno");
            $("#apellido_paterno").focus();
            return false;
        } else if ($("#apellido_materno").val() == "") {
            alert("Ingrese Apellido Materno");
            $("#apellido_materno").focus();
            return false;
        } else if ($("#rfc").val() == "") {
            alert("Ingrese RFC");
            $("#rfc").focus();
            return false;
        } else {

            var dataString = 'id_cliente=' + $('#id_cliente').val() + '&nombre=' + $('#nombre').val() + '&apellido_paterno=' + $('#apellido_paterno').val() + '&apellido_materno=' + $('#apellido_materno').val() + '&rfc=' + $('#rfc').val();
            $.ajax({
                type: "POST",
                url: "editcliente.jsp",
                data: dataString,
                beforeSend: function () {
                    $(".before").show();
                    $(".before").html("<strong>Enviando datos</strong>");
                }, success: function (data) {
                    if (data == 1) {
                        $(".before").hide();
                        $(".sucess").show();
                        $(".sucess").html("<strong>Bien Hecho. El cliente ha sido actualizado correctamente</strong>");
                        /*setTimeout(function () {
                         window.location = "SeleccionarClientes.jsp";
                         }, 3000);*/
                    } else {
                        $(".before").hide();
                        $(".ojo").show();
                        $(".ojo").html(data);
                    }
                }, error: function () {
                    $(".before").hide();
                    $(".sucess").hide();
                    $(".error").show();
                    $(".error").html("<strong>Ocurrio un error</strong>");
                }
            });
            return false;
        }
    });

    $('#cancelar').click(function () {
        setTimeout(function () {
            window.location.href = "SeleccionarClientes.jsp";
        }, 1000);
    });


});
   