// Toggle Function
/*
$('.toggle').click(function () {
    // Switches the Icon
    $(this).children('i').toggleClass('fa-pencil');
    // Switches the forms  
    $('.form').animate({
        height: "toggle",
        'padding-top': 'toggle',
        'padding-bottom': 'toggle',
        opacity: "toggle"
    }, "slow");
});
*/
$(document).ready(function () {
    $(".formulariosddd").submit(function () {
        if ($(".Username").val().length < 1) {
            alert("El campo username es obligatorio");
            $(".Username").focus();
            return false;
        } else if ($(".Password").val().length < 1) {
            alert("El campo password es obligatorio");
            $(".Password").focus();
            return false;
        } else {
            alert("Todo bien")
        }
        return false;
    });
});


$(document).ready(function () {
    $(".before").hide();
    $(".sucess").hide();
    $(".error").hide();
    $(".ojo").hide();
    $('.login').click(function () {
        if ($('.Username').val() == "") {
            alert("Ingrese Username");
            $(".Username").focus();
            return false;
        } else if ($(".Password").val() == "") {
            alert("Ingrese Password");
            $(".Password").focus();
            return false;
        } else {
            var dataString = 'Username=' + $('.Username').val() + '&Password=' + $('.Password').val();
            $.ajax({
                type: "POST",
                url: "respuesta_login.jsp",
                data: dataString,
                beforeSend: function () {
                    $(".ojo").hide();
                    $(".error").hide();
                    $(".before").show();
                    $(".before").html("<strong>Enviando datos</strong>");
                }, success: function (data) {
                    if (data == 1) {
                        $(".before").hide();
                        $(".sucess").show();
                        $(".sucess").html("<strong>Iniciando Sesion</strong>");
                        setTimeout(function () {
                            window.location = "inicio.jsp";
                        }, 3000);
                    } else {
                        $(".before").hide();
                        $(".ojo").show();
                        $(".ojo").html("<strong>No se encontro usuario, Intente de nuevo porfavor</strong>");
                    }
                }, error: function () {
                    $(".before").hide();
                    $(".sucess").hide();
                    $(".error").show();
                    $(".error").html("<strong>Ocurrio un error</strong>");
                }
            });
            return false;
        }
    });

});
   