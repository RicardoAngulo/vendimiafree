<%@ page import="java.io.*,java.util.*,java.net.*,java.sql.*" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.SQLException"%>
<%@ page import="java.sql.PreparedStatement"%>
<%@ include file="inicio.jsp" %>
<html>
    <head>
        <title>Vendimia</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="css/autocomplete.css" />
        <link rel="stylesheet" type="text/css" href="css/DivValidation.css">
        <link rel="stylesheet" type="text/css" href="css/input.css">
    </head>
    <body>
        <div class="diventas">Registro de ventas</div>
        <div class="diventascuerpo">
            <div class="form-style-2">
                <form action="" method="post">
                    <label for="field1"><span>Cliente <span class="required"></span></span><input type="text" class="input-field" name="field1" id="nombre_cliente" value="" placeholder="Buscar Cliente" /></label>
                    <label for="field2">
                        <span>Articulo <span class="required"></span></span>
                        <input type="text" class="input-field" name="field2" value="" placeholder="Buscar Articulo" id="nombre_producto"/>
                    </label>
                    <div class="form-style-2-heading"></div>
                    <label><span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span><input type="submit" class="grabar" value="+" style="position: absolute; top: 240px; left:505px;" /></label>
                    <div class="before"></div>
                    <div class="ojo"></div>
                    <div class="sucess"></div>
                    <div class="error"></div>
                    <%@ include file="venta_detalle.jsp" %>
                </form>
            </div>
        </div>
        <script src='js/jquery.js'></script>
        <script src='js/jquery-1.4.2.min.js'></script>
        <script src="js/jquery.autocomplete.js"></script>
        <script src="js/venta_detalle.js"></script>
        <script language="javascript" type="text/javascript">

            jQuery(function () {
                $("#nombre_cliente").autocomplete("url.jsp");
            });
            jQuery(function () {
                $("#nombre_producto").autocomplete("autocomplete_productos.jsp");
            });
            jQuery(function () {
                $("#id_producto").autocomplete("id_productos.jsp");
            });

        </script>
    </body>
</html>
