<%@ page import="java.sql.*" %>
<% //variables para realizar la conexión a la base de datos 
String user = "root";
String password = "";
String host = "localhost";
String db = "ejercicios";
String url = "jdbc:mysql://"+host+"/"+db;
Connection conn = null;
Statement statement = null;
ResultSet rs = null;

try{

Class.forName("com.mysql.jdbc.Driver").newInstance ();
conn = DriverManager.getConnection(url, user, password);
statement = conn.createStatement();
//inmediatamente hacemos una consulta sencilla
//creamos la consulta
rs = statement.executeQuery("SELECT * FROM mitabla");
//leemos la consulta
while(rs.next()) {
//mostramos los resultados obtenidos
out.println(rs.getString("nombre"));
}
//cerramos la conexión
rs.close();
}catch(SQLException error) {
out.print("Error de Conexión : "+error.toString());
} %>